const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
    plugins: [
        require('flowbite/plugin'),
    ],
  theme: {
    extend: {
      colors: {
        primary: defaultTheme.colors.green,
        secondary: defaultTheme.colors.gray,
        p1:'#222222',
        p2:'#FFFFFF', 
        p3:'#1C5D99', 
        p4:'#639FAB', 
        htxt:'#EABA6B',
        stxt:'#2B303A',
        ptxt:'#3D3241'
      }
    }
  }
}
